package com.example.task04;
import java.util.function.BiFunction;
import java.util.Map;
public class Task04 {
    private static Map<String,BiFunction<Float, Float, Float>> arr=Map.of("+",(c, d)->c+d,
            "-",(c,d)->c-d,
            "*",(c,d)->c*d,
            "/",(c,d)->(d==0?0:c/d));

    public static float calculate(int a, int b, String operation) {
        float ans =arr.get(operation).apply((float)a,(float)b);
        return ans;
    }

    public static void main(String[] args) {
        // Здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        float result = calculate(-25, 5, "/");
        System.out.println(result);
    }

}
