package com.example.task06;
public class Task06 {

    public static int solution(int x, int y) {

        int z=Math.abs(x+y);

        int ans=z==0?1:0;
        while(z>0)
        {
            ans++;
            z/=10;
        }

        return ans;
    }

    public static void main(String[] args) {
        // Здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:

        int result = solution(-111, -222);
        System.out.println(result);

    }

}
