package com.example.task10;

import java.text.DecimalFormat;
import java.util.Objects;

public class Task10 {

    private static String multiply(String pattern,int count)
    {
        String ans=pattern;
        for(int i=1;i<count;i++)
        {
            ans+=pattern;
        }
        return ans;
    }
    public static boolean compare(float a, float b, int precision) {

        if(a==1/0.0 && b==1/0.0) {
            return true;
        }
        if(a==1/0.0 || b==1/0.0) //к этому моменту вариант, что они оба NaN обработаны
        {
            return false;
        }

        if(a==-1/0.0 && b==-1/0.0) {
            return true;
        }
        if(a==-1/0.0 || b==-1/0.0) //к этому моменту вариант, что они оба -NaN обработаны
        {
            return false;
        }

        String df = "%."+String.valueOf(precision==0?0:precision-1)+"f";

        String first = String.format(df,a),second=String.format(df,b);

        return Objects.equals(first, second);

    }

    public static void main(String[] args) {
        float a = 0.3f;
        float b = 0.4f;
        float sum = a + b;
        float c = 0.7f;

        boolean result = compare(100.001f, 100.009f, 2);
        System.out.println(result);

    }

}
