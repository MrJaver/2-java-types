package com.example.task02;

public class Task02 {

    public static String solution(String input) {

        String answer="long";
        long count = Long.parseLong(input);

        if(-2147483648 <= count && count <= 2147483647)
        {
            answer="int";
        }

        if(-32768<= count && count <= 32767)
        {
            answer="short";
        }

        if(-128 <= count && count <= 127)
        {
            answer="byte";
        }

        return answer;
    }

    public static void main(String[] args) {

        String result = solution("12345");
        System.out.println(result);
    }

}
